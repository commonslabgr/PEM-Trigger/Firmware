/*    
 *   PEM-Trigger Firmware
 *   
 *   This is the firmware for a device, which takes a trigger from a
 *   photoelastic modulator and sends it (with a delay) to a laser.
 *   
 *   Copyright (C) 2018 Jann Eike Kruse <jann@commonslab.gr>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#define ENCODER_DO_NOT_USE_INTERRUPTS
#include <Encoder.h>
#include <U8g2lib.h>
U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R0, /* clock=*/ 47, /* data=*/ 49, /* CS=*/ 51, /* reset=*/ 43);

#include "splash.xbm.h"
#include "BNCs.xbm.h"

const            byte triggerOutPin = 12;
const            byte triggerInPin  = 13;
const            byte triggerStatusPin  = 70;
const            byte triggerEnablePin  = 11;

const            byte LcdRef3v3Pin = 22;

const            byte encoder1HighPin = 23;
const            byte encoder1LowPin = 24;
const            byte encoder1buttonPin = 25;
const            byte encoder1aPin = 26;
const            byte encoder1bPin = 27;

const            byte encoder2HighPin = 28;
const            byte encoder2LowPin = 29;
const            byte encoder2aPin = 30;
const            byte encoder2bPin = 31;
const            byte encoder2buttonPin = 32;

const            byte LcdPsbPin = 45;
const            byte LcdVddPin = 42;

             uint16_t divider = 5000; // Default divider: 50kHz / 5000 = 10Hz
                 char dividerChar[] = "65500";
volatile     uint16_t counter = 0;
const         uint8_t duty =  166;    // Corresponds to about 10us
             uint16_t delayA =  0;
             uint16_t delayB =  0;
             uint16_t delayNext =  0;
             uint16_t delayCounter =  0;
const        uint16_t delayMax =  999;
                 char delayChar[] = "999";
volatile         bool updateDisplay = true;
                 bool enableA = true;
                 bool enableB = true;
volatile         bool enableEither = true;
                 bool editDivider = false;
volatile     uint32_t lastMicrosA;
volatile     uint32_t lastMicrosB;
volatile        float period;
                 char freqencyChar[] = "1000.0";
volatile         bool pulseA = true;

              Encoder encoder1(encoder1aPin, encoder1bPin);
              Encoder encoder2(encoder2aPin, encoder2bPin);

void setup() {

  pinMode(encoder1buttonPin, INPUT_PULLUP);
  pinMode(encoder2buttonPin, INPUT_PULLUP);

  // Select SERIAL mode on pin PSB
  pinMode(LcdPsbPin, OUTPUT);
  digitalWrite(LcdPsbPin, LOW);

  // Provide reference for contrast
  pinMode(LcdRef3v3Pin, OUTPUT);
  digitalWrite(LcdRef3v3Pin, HIGH);

  // Supply voltage for logic circuit
  pinMode(LcdVddPin, OUTPUT);
  digitalWrite(LcdVddPin, HIGH);

  // High Reference for encoder1
  pinMode(encoder1HighPin, OUTPUT);
  digitalWrite(encoder1HighPin, HIGH);
  pinMode(encoder1LowPin, OUTPUT);
  digitalWrite(encoder1LowPin, LOW);

  // High Reference for encoder2
  pinMode(encoder2HighPin, OUTPUT);
  digitalWrite(encoder2HighPin, HIGH);
  pinMode(encoder2LowPin, OUTPUT);
  digitalWrite(encoder2LowPin, LOW);

  // 3.3V Reference for level shifter
  digitalWrite(triggerEnablePin, HIGH);
  pinMode(triggerEnablePin, OUTPUT);

  digitalWrite(triggerOutPin, HIGH);
  pinMode(triggerOutPin, OUTPUT);

  pinMode(triggerInPin, INPUT_PULLUP);
  attachInterrupt(triggerInPin, interruptFunction, RISING);

  u8g2.begin();

  encoder1.write(400);
  encoder2.write(800);

  pinMode(triggerStatusPin, OUTPUT);

  u8g2.clearBuffer();          // clear the internal display buffer memory
  u8g2.drawXBMP( 0, 0, splash_width, splash_height, splash_bits);
  u8g2.sendBuffer();          // transfer internal memory to the display
  delay(5000);

  u8g2.clearBuffer();          // clear the internal display buffer memory
  u8g2.drawXBMP( 0, 0, BNCs_width, BNCs_height, BNCs_bits);
  u8g2.sendBuffer();          // transfer internal memory to the display
  delay(5000);

}

void loop() {
  // ----- DEMO: Start with default delay (0) and react to encoder.
  while (true) {
    // wait...
    for (int i=0; i<10000; i++) {
      if (encoder1.read() < 0) encoder1.write(0);
      if (encoder2.read() < 0) encoder2.write(0);
      delayMicroseconds(10);
    }
      if (digitalRead(encoder1buttonPin) == LOW) {
        for (int i=0; ((digitalRead(encoder1buttonPin) == LOW) && i<=1000); i++) {
          delayMicroseconds(1000); 
        }
        if (digitalRead(encoder1buttonPin) == LOW) {
          if (editDivider == false) {
            editDivider = true;
            encoder1.write(divider/50*4);
          } else {
            editDivider = false;
            encoder1.write(delayA*4);            
          }
          refreshDisplay();
          
          while (digitalRead(encoder1buttonPin) == LOW) {}
        } else {
            if (enableA == false) {
              enableA = true;
            } else {
              enableA = false;
              enableB = true; // Never disable both A and B.
            }
//          enableA = !enableA;
//          enableEither = enableA || enableB;
          refreshDisplay();
        }
        delayMicroseconds(1000);
      }

      if (digitalRead(encoder2buttonPin) == LOW) {
        while (digitalRead(encoder2buttonPin) == LOW) { delayMicroseconds(1); }
          if (enableB == false) {
            enableB = true;
          } else {
            enableB = false;
            enableA = true; // Never disable both A and B.
          }
//        enableB = !enableB;
//        enableEither = enableA || enableB;
        refreshDisplay();
        delayMicroseconds(1000);
      }

    

    if (editDivider == false) {
      if  (delayA != encoder1.read()/4) {
        if  (encoder1.read() > delayMax*4) encoder1.write(delayMax*4);
        delayA = encoder1.read()/4;
        refreshDisplay();
        encoder1.write(delayA*4);
      }
    } 

    if (editDivider == true) {
      if  (divider != encoder1.read()/4 * 50) {
        if  (encoder1.read() > 4000) encoder1.write(4000);
        if  (encoder1.read() < 4) encoder1.write(4);
        divider = encoder1.read()/4 * 50;
        refreshDisplay();
        encoder1.write(divider/50*4);
      }
    }

    if  (delayB != encoder2.read()/4) {
      if  (encoder2.read() > delayMax*4) encoder2.write(delayMax*4);
      delayB = encoder2.read()/4;
      refreshDisplay();
      encoder2.write(delayB*4);
    }
  }
}

void refreshDisplay() {
  u8g2.clearBuffer();          // clear the internal display buffer memory
  u8g2.setFont(u8g2_font_unifont_t_latin); // choose a medium-sized font
  u8g2.drawStr(0,10,"Delay A");
  u8g2.drawStr(73,10,"Delay B");
  
  u8g2.setFont(u8g2_font_logisoso28_tr); // choose a LARGE font
  
  if (enableA == true) {
    sprintf(delayChar,"%3u",delayA); // “print” delayA as a decimal number into the string delayChar */
    u8g2.drawStr(0,42,delayChar);
  } else {
    u8g2.drawStr(0,42,"OFF");
  }
  
  if (enableB == true) {
    sprintf(delayChar,"%3u",delayB); // “print” delayB as a decimal number into the string delayChar */
    u8g2.drawStr(74,42,delayChar);
  } else {
    u8g2.drawStr(74,42,"OFF");
  }
  
  u8g2.setFont(u8g2_font_mozart_nbp_tr); // choose small font
  sprintf(dividerChar,"%5u",divider); // “print” divider as a unsigned number into the string buffer */
  u8g2.drawStr(0,62,"Divider:");
  u8g2.drawStr(45,62,dividerChar);
  
  if (editDivider == true) {
   u8g2.drawLine(45, 63, 75, 63);
  }
  
  sprintf(freqencyChar,"%6.1f",10000/period); // “print” frequency as a floating-point number into the string buffer */
  u8g2.drawStr(80,62,freqencyChar);
  u8g2.drawStr(116,62,"Hz");

  u8g2.sendBuffer();          // transfer internal memory to the display
  updateDisplay = false;
}

void interruptFunction() {
  // The following code needs precice timing, 
  // so we don't want any interruptions.
  noInterrupts();
  detachInterrupt(triggerInPin);
  
  // Check if enough input pulses have passed:
  if (counter == divider) if (enableEither) {

    // Delay:
    while (delayCounter<delayNext){
    delayCounter++;
    __asm__("nop\n\t");
    __asm__("nop\n\t");
    };

    // Send the trigger:
    PIOD->PIO_SODR = PIO_PD8; // SetOutputDataRegister (SODR): PD8 corresponds to ARDUINO Pin 12 on DUE

    // Keep the trigger on for a specific duty cycle:
    for (uint8_t j=0;j<duty;j++){
        __asm__("nop\n\t"); 
    };

    // Set the trigger level back to LOW
    PIOD->PIO_CODR = PIO_PD8; //ClearOutputDataRegister (CODR): PD8 corresponds to ARDUINO Pin 12 on DUE

    // Start counting the input pules again from the start:
    counter=0;
    delayCounter=0;

    // Toggle between delayA and delayB
    if (pulseA == true) {
      if (enableB == true) {
        delayNext = delayB;
        pulseA = false;
        period = (micros() - lastMicrosA) /99.82375 /2 -0.02;
        lastMicrosA = micros();
      }
      if (enableB == false) {
        delayNext = delayA;
        pulseA = true;
        period = (micros() - lastMicrosA) /99.82375 -0.02;
        lastMicrosA = micros();
      }
    } else {
      if (enableA == true) {
        delayNext = delayA;
        pulseA = true;
        period = (micros() - lastMicrosB) /99.82375 /2 -0.02;
        lastMicrosB = micros();
      }
      if (enableA == false) {
        delayNext = delayB;
        pulseA = false;
        period = (micros() - lastMicrosB) /99.82375 -0.02;
        lastMicrosB = micros();
      }
    }
  }
  
  if (counter == (divider/2) ) {
    digitalWrite(triggerStatusPin, pulseA);
  }

  // Keep counting:
  counter++;
  attachInterrupt(triggerInPin, interruptFunction, RISING);

  // Enable interrupts, so we can continue.
  interrupts();
}
